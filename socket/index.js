import * as config from "./config";

const rooms = [];
const players = [];

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;

    socket.on("JOIN_TO_GAME", () => {
      if (players.length <= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        const player = {
          name: username,
          id: socket.id,
        };
        players.push(player);
      }
      socket.emit("ADD_PLAYER", players);
    });

    socket.on("DISCONNECT", () => {
      players = players.filter((player) => player.id != socket.id);
    });
  });
};
