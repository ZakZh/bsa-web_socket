import { createElement } from "./helpers/domHelper.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", {
  query: { username },
});

const gamePage = document.getElementById("game-page");
const roomsPage = document.getElementById("rooms-page");
gamePage.style.display = "block";
roomsPage.style.display = "none";

const gameMenu = createElement({
  tagName: "div",
  id: "game-menu",
  children: [
    {
      tagName: "h3",
      id: "in-game-room-name",
      textContent: "Room",
    },
    {
      tagName: "button",
      id: "disconnect-game-btn",
      textContent: "Out",
    },
    {
      tagName: "div",
      id: "player-list",
    },
  ],
});

gamePage.appendChild(gameMenu);

const disconnectBtn = document.getElementById("disconnect-game-btn");

disconnectBtn.addEventListener("click", () => {
  socket.emit("DISCONNECT");
});

const playerList = document.getElementById("player-list");

const gameWindow = createElement({
  tagName: "div",
  id: "game-window",
  children: [
    {
      tagName: "button",
      id: "game-ready-btn",
      textContent: "Ready",
    },
  ],
});

gamePage.appendChild(gameWindow);

socket.emit("JOIN_TO_GAME");

const addPlayer = (players) => {
  players.forEach((player) => {
    const playerInList = createElement({
      tagName: "div",
      className: "players-in-list",
      id: player.id,
      children: [
        {
          tagName: "span",
          className: "player-status",
        },
        {
          tagName: "p",
          className: "player-name",
          textContent: player.name,
        },
        {
          tagName: "div",
          className: "player-progress-bar",
          children: [
            {
              tagName: "div",
              className: "player-progress",
            },
          ],
        },
      ],
    });

    playerList.appendChild(playerInList);
  });
};

socket.on("ADD_PLAYER", addPlayer);
