function addChildrenToElement(children) {
    const childrenElements = children.map(child => {
        const childElement = createElement(child);
        return childElement;
    });
    return childrenElements;
}
export function createElement(el) {
    const element = document.createElement(el.tagName);
    if (el.id)
        element.id = el.id;
    if (el.className) {
        const classNames = el.className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }
    if (el.attributes !== undefined) {
        Object.keys(el.attributes).forEach((key) => {
            if (el.attributes)
                element.setAttribute(key, el.attributes[key]);
        });
    }
    if (el.textContent)
        element.innerText = el.textContent;
    if (el.children && el.children.length > 0) {
        const childrenOf = addChildrenToElement(el.children);
        element.append(...childrenOf);
    }
    return element;
}
