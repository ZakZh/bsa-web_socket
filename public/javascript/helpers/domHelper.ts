type attributesType = {
  [key: string]: string
}

interface htmlElementInt {
  id?:string;
  tagName: string;
  className?:string;
  attributes?:attributesType;
  textContent?:string;
  children?:htmlElementInt[];
}

function addChildrenToElement(children:htmlElementInt[]):HTMLElement[] {
  const childrenElements = children.map(
    child => {
    const childElement:HTMLElement = createElement(child);
    return childElement;
  })
  return childrenElements;
}

export function createElement(el:htmlElementInt):HTMLElement {
  const element: HTMLElement = document.createElement(el.tagName);

  if (el.id) element.id = el.id;

  if (el.className) {
    const classNames = el.className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (el.attributes !== undefined) {
    Object.keys(el.attributes).forEach((key:string) => {
      if (el.attributes) element.setAttribute(key, el.attributes[key])
    }
    );
  }

  if (el.textContent) element.innerText = el.textContent;

  if (el.children && el.children.length > 0) {
     const childrenOf:HTMLElement[] = addChildrenToElement(el.children)

    element.append(...childrenOf);
  }
  return element;
}
